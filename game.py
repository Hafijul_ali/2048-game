import PySimpleGUI as gui

from board import Board
from constant import LOGO, INTRO
from controls import Control
from ui import UI


class Game:
    key_alnum = {
        "UP": ("8", "W", "UP"),
        "DOWN": ("2", "S", "DOWN"),
        "LEFT": ("4", "A", "LEFT"),
        "RIGHT": ("6", "D", "RIGHT"),
        "END": ("0", "Q"),
        "HELP": "5"
    }

    def __init__(self) -> None:
        self.board = Board(size=4)
        self.input = Control(game=self)
        self.ui = UI(data=self.board)
        self.command = Game.key_alnum
        self.board.occupy_cell()
        self.board.occupy_cell()
        self.ui.display()

    def run(self) -> None:
        self.ui.un_hide()
        while not self.over():
            event, values = self.ui.read(timeout_key='-time-out-', timeout=1000)
            if event in (gui.WIN_CLOSED, "Exit"):
                break
            if event == "Restart Game":
                self.restart()
            if event == "Change Theme":
                self.ui.change_theme()
            if event == "-time-out-": 
                continue
            if event == "Help":
                self.show_help()
            if ":" in event:
                key = event.split(":")[0].upper()
                self.input.handle(key)
                self.board.occupy_cell()
                self.ui.update_score()
                self.ui.display()
        gui.popup("Game over, your max score was :", self.board.max_score())

    def over(self) -> bool:
        if self.board.empty_cell_count() == 0:
            return True
        return False

    def restart(self):
        self.ui.refresh()

    def show_help(self):
        gui.popup_annoying(LOGO)
