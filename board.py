import os
from random import randint, choice


class Board:

    def __init__(self, size) -> None:
        self.size = size
        self.empty_cells = size*size
        self.board = [[0 for x in range(self.size)]for y in range(self.size)]

    def move_up(self) -> None:
        j = 0
        while j < 4:
            i = 0
            while i < 8:
                if self.board[i % 3][j] == self.board[i % 3+1][j]:
                    self.board[i % 3][j] *= 2
                    self.board[i % 3+1][j] = 0
                elif self.board[i % 3][j] == 0 and self.board[i % 3+1][j] != 0:
                    self.board[i % 3][j], self.board[i %
                                                     3+1][j] = self.board[i % 3+1][j], 0
                i += 1
            j += 1

    def move_down(self) -> None:
        j = 0
        while j < 4:
            i = 0
            while i < 8:
                if self.board[i % 3][j] == self.board[i % 3+1][j]:
                    self.board[i % 3][j] = 0
                    self.board[i % 3+1][j] *= 2
                elif self.board[i % 3][j] != 0 and self.board[i % 3+1][j] == 0:
                    self.board[i % 3+1][j], self.board[i %
                                                       3][j] = self.board[i % 3][j], 0
                i += 1
            j += 1

    def move_left(self) -> None:
        i = 0
        while i < 4:
            j = 0
            while j < 8:
                if self.board[i][j % 3] == self.board[i][j % 3+1]:
                    self.board[i][j % 3] *= 2
                    self.board[i][j % 3+1] = 0
                elif self.board[i][j % 3] == 0 and self.board[i][j % 3+1] != 0:
                    self.board[i][j % 3], self.board[i][j %
                                                        3+1] = self.board[i][j % 3+1], 0
                j += 1
            i += 1

    def move_right(self) -> None:
        i = 0
        while i < 4:
            j = 0
            while j < 8:
                if self.board[i][j % 3] == self.board[i][j % 3+1]:
                    self.board[i][j % 3] = 0
                    self.board[i][j % 3+1] *= 2
                elif self.board[i][j % 3] != 0 and self.board[i][j % 3+1] == 0:
                    self.board[i][j % 3+1], self.board[i][j %
                                                          3] = self.board[i][j % 3], 0
                j += 1
            i += 1

    def empty_cell_count(self) -> int:
        cell_count = 0
        for i in range(self.size):
            cell_count += self.board[i].count(0)
        return cell_count

    def occupy_cell(self) -> None:
        (x, y) = randint(0, self.size-1), randint(0, self.size-1)
        if self.board[x][y] == 0:
            self.board[x][y] = choice([2, 4])
            self.empty_cells -= 1
        elif self.empty_cells != 0:
            self.occupy_cell()

    # def display(self) -> None:
    #     pad = 2*len(str(self.max_score()))+1
    #     dash = (pad+2)*self.size + (self.size-1)
    #     for i in range(self.size):
    #         print("-"*dash)
    #         print(*["|{:^{pad}}|".format(self.board[i][j], pad=pad)
    #               for j in range(self.size)])
    #         print("-"*dash)

    # def wipe(self) -> None:
    #     if os.name == "nt":
    #         os.system('cls')
    #     else:
    #         os.system('clear')

    def max_score(self) -> int:
        return max(map(max, self.board))
