# 2048 Game


## Description

Command line Interface version of popular 2048 Game written in python and PySimpleGUI for Interface.


## Visuals

**look of the game.**
 ![Glimpse](./ss.png)


## Installation

The repository can be 

-  cloned by `git clone https://gitlab.com/Hafijul_ali/2048-game.git`
-  change directory using `cd 2048-game`
-  install the depencies using `pip install -r requirements.txt`


## Usage

The game is fairly simple to use, use arrow keys or a,s,d,w controls to move the numbers in the specified direction. A web-based version of the game can be played at [Live](https://replit.com/@HafijulAli/2048-Game?v=1)


## Making changes 

```
cd existing_repo
git remote add origin https://gitlab.com/Hafijul_ali/2048-game.git
git branch -M main
git push -uf origin main
```


## Authors and acknowledgment

This project is solely work of their author(s), any external library used has been properly mentioned in the `requirements.txt` file.


## License

This project is licensed under [Apache License2.0](https://apache.org/licenses/LICENSE-2.0) .


## Project status

This project is a work in progress and changes to the code base is done depending upon author(s)' availability. Any code changes will be reflected in Git Commit History.
