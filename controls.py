

class Control:
    def __init__(self, game) -> None:
        self.game = game

    def handle(self, key):
        if key in self.game.command["UP"]:
            self.game.board.move_up()
        elif key in self.game.command["DOWN"]:
            self.game.board.move_down()
        elif key in self.game.command["LEFT"]:
            self.game.board.move_left()
        elif key in self.game.command["RIGHT"]:
            self.game.board.move_right()
        elif key in self.game.command["HELP"]:
            self.game.show_help()
            if key == self.game.command["END"]:
                pass
        elif key == self.game.command["END"]:
            return