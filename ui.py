import PySimpleGUI as gui


class UI(gui.Window):
    THEME = "Material 2"
    WINDOW_WIDTH = WINDOW_HEIGTH = 600
    SIZE = 4

    def __init__(self, data) -> None:
        self.data = data
        gui.theme(UI.THEME)
        super().__init__("2048 Game",
                         self.build_layout(),
                         size=(UI.WINDOW_WIDTH, UI.WINDOW_HEIGTH),
                         default_element_size=(UI.SIZE * 2, UI.SIZE),
                         default_button_element_size=(UI.SIZE * 2, UI.SIZE),
                         auto_size_buttons=False,
                         element_justification="center",
                         finalize=True,
                         return_keyboard_events=True)
        self.hide()

    def build_layout(self) -> list:
        menu = [
            ["File", ["Save", "Load", "Exit"]],
            [
                "Options",
                ["Change Player Name", "Change Theme", "Restart Game"]
            ],
            ["About", ["Help", "Rules", "Contact"]],
        ]
        layout = [
            [gui.Menu(menu_definition=menu)],
            [
                gui.Text(
                    "2048 Game",
                    font=("Arial", 20),
                    size=(UI.WINDOW_WIDTH, 1),
                    justification="center",
                )
            ],
            [
                gui.Text(
                    "Score: 0",
                    font=("Arial", 20),
                    size=(UI.WINDOW_WIDTH, 1),
                    justification="center",
                    key="-scoreboard-",
                )
            ],
        ]
        layout += [[
            gui.Button(self.data.board[x][y],
                       auto_size_button=False,
                       key=(x, y),
                       button_color=("Yellow", ""),
                       font=("Arial", 13, "bold"))
            for y in range(len(self.data.board))
        ] for x in range(len(self.data.board))]
        return layout

    def update_score(self) -> None:
        self["-scoreboard-"].update("Score: {0}".format(self.data.max_score()))

    def display(self) -> None:
        [[
            self[(x, y)].update(self.data.board[x][y])
            for x in range(len(self.data.board))
        ] for y in range(len(self.data.board))]

    def change_theme(self):
        theme_window = gui.Window(
            "Theme Chooser",
            [
                [
                    gui.Text("Select Theme:"),
                    gui.Combo(
                        values=gui.theme_list(),
                        size=(30, 1),
                        enable_events=True,
                        key="Theme",
                    ),
                ],
                [gui.Ok(), gui.Cancel()],
            ],
            size=(450, 450),
            auto_size_text=True,
        )
        while True:
            ui_event, theme_values = theme_window.read()
            if ui_event in ("Close", gui.WINDOW_CLOSED):
                break
            if ui_event == "Ok":
                Theme = theme_values["Theme"]
                break
        theme_window.close()
        gui.theme(Theme)
