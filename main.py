from game import Game

if __name__ == "__main__":
    try:
        game = Game()
        game.run()
    except Exception as e:
        pass
    finally:
        game.ui.close()
